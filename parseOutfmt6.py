#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# GitLab: MachadoDJ & DachBrown
# parseOutfmt6.py
# Gets a .tsv table of results from Blast (output format 6) and a genes .tsv table as well.
# Returns best hit per position (unique overlapping positions per sseqid).
# Note: sstart and ssend will always be reported in order (i.e., sstart < ssend).
##

##
# Import libraries
##

import argparse, re, sys

try:
	from Bio import SeqIO
	from Bio.Seq import Seq
	# from Bio.Alphabet import generic_dna
except:
	sys.stderr.write("! ERROR: Could not load Biopython.\n")
	exit()

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()

parser.add_argument("-b", "--blast_results", help = "BLAST results in output format 6", type = str, required = True)
parser.add_argument("-e", "--evalue", help = "E-value threshold (default = 1e-03)", type = float, default = 1e-03, required = False)
parser.add_argument("-g", "--genes_table", help = "Gene information in tab-separated values (Accession, Nickname, and Description). Not required, used to generate basic statistics only.", type = str, required = False)
parser.add_argument("-o", "--output", help = "Prefix for output files (default = 'output')", type = str, default = "output", required = False)
parser.add_argument("-s", "--sorting_order", help = "Space-separeted list of Columns to be sorted, in order, starting from 0 (default = 3 2 11 10)", type = int, nargs = "+", default = [3, 2, 11, 10], required = False)
parser.add_argument("-t", "--target", help = "Path to target DNA sequence(s) in FASTA format", type = str, required = True)
parser.add_argument("-v", "--verbose", help="Increase output verbosity", action="store_true", required = False)
args = parser.parse_args()

##
# Define functions
##

def parse_genes(filename):
	dic = {}
	handle = open(filename, "r")
	for line in handle.readlines():
		line = re.sub("#.*", "", line.strip())
		if(line):
			try:
				accession, nickname, description = line.split("\t")
			except:
				sys.stderr.write("! Error parsing file {}.\n".format(filename))
				exit()
			else:
				# sys.stdout.write("{}, {}, {}\n".format(accession, nickname, description))
				dic[accession] = [nickname, description]
	handle.close()
	return dic



def parse_blast_results(filename):
	dic = {}
	handle = open(filename, "r")
	for line in handle.readlines():
		line = re.sub("#.*", "", line.strip())
		if(line):
			try:
				qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = line.split("\t")
				pident = float(pident)
				length = int(length)
				mismatch = int(mismatch)
				gapopen = int(gapopen)
				qstart = int(qstart)
				qend = int(qend)
				sstart = int(sstart)
				send = int(send)
				evalue = -1 * float(evalue) # This multiplication helps with sorting later
				bitscore = float(bitscore)
			except:
				sys.stderr.write("! Error parsing file {}.\n".format(filename))
				exit()
			else:
				# sys.stdout.write("{}\n".format(qseqid))
				if(sseqid in dic and float(evalue) <= args.evalue):
					try:
						qseqid = re.compile(".+\|(.+)\|.+").findall(qseqid)[0]
					except:
						pass
					dic[sseqid] += [[qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore]]
				elif(float(evalue) <= args.evalue):
					try:
						qseqid = re.compile(".+\|(.+)\|.+").findall(qseqid)[0]
					except:
						pass
					dic[sseqid] = [[qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore]]
				else:
					sys.stderr.write("REJECTED:\n{}\n".format("\t".join([qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore])))
					pass
	handle.close()
	return dic



def report_basic_results(genes_dic, results_dic):
	if(args.verbose): sys.stdout.write("## Summary:\n")
	for sseqid in results_dic:
		# sys.stdout.write("{}: {}\n".format(sseqid, len(results_dic[sseqid])))
		uniq_qseqid = []
		uniq_nicknames = []
		uniq_descriptions = []
		for list in results_dic[sseqid]:
			qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = list
			nickname = genes_dic[qseqid][0]
			description = genes_dic[qseqid][1]
			if(not qseqid in uniq_qseqid):
				uniq_qseqid += [qseqid]
			if(not nickname in uniq_nicknames):
				uniq_nicknames += [nickname]
			if(not description in uniq_descriptions):
				uniq_descriptions += [description]
		if(args.verbose): sys.stdout.write("# {} = {} accessions (nicknames = {}, descriptions = {})\n".format(sseqid, len(uniq_qseqid), len(uniq_nicknames), len(uniq_descriptions)))
	if(args.verbose): sys.stdout.write("##\n")
	return



def get_overlapping_positions(results_dic):
	if(args.verbose): sys.stdout.write("## Positions:\n")
	pos = {}
	for sseqid in results_dic:
		dic_per_sseqid = {}
		for list in results_dic[sseqid]:
			qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = list
			s = int(sstart)
			e = int(send)
			add = True
			for key in dic_per_sseqid:
				i = dic_per_sseqid[key][0]
				j = dic_per_sseqid[key][1]
				x = range(min(s, e), max(s, e) + 1)
				y = range(min(i, j), max(i, j) + 1)
				xs = set(x)
				if(len(xs.intersection(y)) >= 1):
					add = False
					dic_per_sseqid[key] = [min(s, i), max(e, j)]
					break
			if(add == True):
				dic_per_sseqid[len(dic_per_sseqid)] = [s, e]
		pos[sseqid] = dic_per_sseqid
		if(args.verbose): sys.stdout.write("# {} {}\n".format(sseqid, dic_per_sseqid))
	if(args.verbose): sys.stdout.write("##\n")
	return pos



def get_best_hits_per_position(results_dic, pos):
	best = []
	if(args.verbose): sys.stdout.write("## Best hits per position:\n# qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\n")
	for sseqid in results_dic:
		hits = [i for i in results_dic[sseqid]]
		for i in args.sorting_order:
			hits.sort(key = lambda x: x[i], reverse = True)
		for key in pos[sseqid]:
			i, j = pos[sseqid][key]
			for line in hits:
				s = line[8]
				e = line[9]
				x = range(min(s, e), max(s, e) + 1)
				y = range(min(i, j), max(i, j) + 1)
				xs = set(x)
				if(len(xs.intersection(y)) >= 1):
					try:
						line[10] = "{:.2e}".format(-1 * line[10]) # This helps reading the results
					except:
						pass
					if(not line in best):
						best.append(line)
						sys.stdout.write("{}\n".format("\t".join([str(k) for k in line])))
					break
	if(args.verbose): sys.stdout.write("##\n")
	return best



def makeSequenceMultipleOfThree(targetSequence, sstart, send):
	seq = targetSequence[sstart - 1 : send]
	diff = len(seq) % 3
	if(diff == 1):
		newSeq = targetSequence[sstart - 1: send + 2]
		if(newSeq == seq):
			newSeq = targetSequence[sstart - 3: send]
			if(newSeq == seq):
				newSeq = targetSequence[sstart:  send]
				sstart += 1
			else:
				sstart -= 2
		else:
			send += 2
	elif(diff == 2):
		newSeq  = targetSequence[sstart - 1: send + 1]
		if(newSeq == seq):
			newSeq   = targetSequence[sstart - 2: send]
			if(newSeq == seq):
				newSeq = targetSequence[sstart + 1: send]
				sstart += 2
			else:
				sstart -= 1
		else:
			send += 1
	return sstart, send



def translateEveryFrame(seq):
	# All possible translations in 5'3' orientation ('seq' may already be reverse complemented):
	# translation_53_frame1 = str(Seq(seq,             generic_dna).translate())
	# translation_53_frame2 = str(Seq(seq[1:-2],       generic_dna).translate())
	# translation_53_frame3 = str(Seq(seq[2:-4],       generic_dna).translate())
	translation_53_frame1 = str(Seq(seq,     ).translate())
	translation_53_frame2 = str(Seq(seq[1:-2]).translate())
	translation_53_frame3 = str(Seq(seq[2:-4]).translate())

	# Get the best frame (select based on the largest contiguous translatable sequence fragment):
	orf = {}
	try:
		orf[1] = sorted(re.compile("([^\*]+)").findall(translation_53_frame1), key=len, reverse=True)[0]
		orf[2] = sorted(re.compile("([^\*]+)").findall(translation_53_frame2), key=len, reverse=True)[0]
		orf[3] = sorted(re.compile("([^\*]+)").findall(translation_53_frame3), key=len, reverse=True)[0]
	except:
		sys.stderr.write("! ERROR: empty sequences!\n")
		sys.stderr.write("5' 3': frame 1: {}\n".format(translation_53_frame1))
		sys.stderr.write("5' 3': frame 2: {}\n".format(translation_53_frame2))
		sys.stderr.write("5' 3': frame 3: {}\n".format(translation_53_frame3))
		exit()
	# Select frame that results in the longest contiguous peptide sequence:
	frame = sorted(orf, key=lambda k: len(orf[k]), reverse=True)[0]
	return frame



def extendTranslation(targetSequence, sstart, send):
	mx = len(targetSequence)
	dna = ""
	pep = ""
	# First, try adding new codons to the right:
	while True:
		codon = targetSequence[send - 3 : send]
		if(len(codon) != 3):
			break # This break may result on send longer than the sequence, indicating incomplete codons
		else:
			try:
				# aa = Seq(codon, generic_dna).translate()
				aa = Seq(codon).translate()
				aa = str(aa)
			except:
				break
			else:
				if(aa == "*"):
					break
				else:
					send += 3
	# If send is greater than the target sequence's length, pad the end with missing data:
	if send > mx:
		targetSequence = "{}{}".format(targetSequence, "N" * (send - mx))
	# Second, try adding new codons to the left:
	while True:
		codon = targetSequence[sstart - 1 : sstart + 2]
		if(len(codon) != 3) or sstart == 1:
			break
		else:
			try:
				# aa = Seq(codon, generic_dna).translate()
				aa = Seq(codon).translate()
				aa = str(aa)
			except:
				break
			else:
				if (aa == "M"):
					break
				elif (aa == "*"): # This was added to make sure stop codons are not included during 3' expansion
					sstart += 3
					break
				else:
					sstart -= 3
	if sstart < 1: # If sstart is smaller than 1, pad the start with missing data:
		dna = targetSequence[0 : send]
		div = len(dna) % 3
		if div != 0:
			dna = "{}{}".format("N" * (3 - div), dna)
	else: # If sstart is greater than one, extract nucleotides as usual:
		dna = targetSequence[sstart - 1 : send]
	# pep = str(Seq(dna, generic_dna).translate())
	pep = str(Seq(dna).translate()) # Get translation
	return sstart, send, dna, pep



def extractUniqueOrfs(best, target):
	# Create output files:
	dnaHits     = open("{}_hits_dna.fasta".format(args.output),     "w")
	dnaAdjusted = open("{}_adjusted_dna.fasta".format(args.output), "w")
	pepAdjusted = open("{}_adjusted_pep.fasta".format(args.output), "w")
	dnaExpanded = open("{}_expanded_dna.fasta".format(args.output), "w")
	pepExpanded = open("{}_expanded_pep.fasta".format(args.output), "w")
	if(args.verbose): sys.stdout.write("## Uniq fragments:\n")
	# Read selected hits and save into dictionary:
	selectedHits = {}
	for hit in best:
		qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = hit
		sstart = int(sstart)
		send   = int(send)
		sseqid = str(sseqid)
		if(sseqid in selectedHits):
			selectedHits[sseqid] += [[sstart, send]]
		else:
			selectedHits[sseqid]  = [[sstart, send]]
	# Read the target sequences and save into dictionary:
	targetSequences = {}
	handle = open(target, "r")
	for record in SeqIO.parse(handle, "fasta"):
		sseqid = str(record.id)
		if(sseqid in selectedHits):
			targetSequences[sseqid] = str(record.seq) # Only unique sequences identifiers are kept
	handle.close()
	# Loop for every uniq position:
	for sseqid in targetSequences:
		targetSequence = targetSequences[sseqid]
		for position in selectedHits[sseqid]:
			sstart = position[0]
			send   = position[1]
			if(args.verbose): sys.stdout.write("# {} [{}:{}]:\n".format(sseqid, sstart, send))
			# Decide on the orientation:
			if(sstart > send):
				orientation = 35
				# targetSequence = str(Seq(targetSequence, generic_dna).reverse_complement())
				targetSequence = str(Seq(targetSequence).reverse_complement())
				sstart = len(targetSequence) - sstart + 1
				send   = len(targetSequence) - send   + 1
			else:
				orientation = 53
			seq    = targetSequence[sstart - 1: send]
			if(orientation == 35):
				textOutput = ">{0} revcomp[{1}:{2}] hit\n{3}\n".format(sseqid, sstart, send, seq)
				if(args.verbose): sys.stdout.write(textOutput)
				dnaHits.write(textOutput)
			else:
				textOutput = ">{0} [{1}:{2}] hit\n{3}\n".format(sseqid, sstart, send, seq)
				if(args.verbose): sys.stdout.write(textOutput)
				dnaHits.write(textOutput)
			diff = len(seq) % 3
			if(diff != 0):
				sstart, send = makeSequenceMultipleOfThree(targetSequence, sstart, send)
				seq          = targetSequence[sstart - 1: send]
			frame = translateEveryFrame(seq)
			if(frame == 2):
				sstart += 1
				send   -= 2
			elif(frame == 3):
				sstart += 2
				send   -= 1
			dnaAdjusted.write(">{0} [{1}:{2}] adjusted\n{3}\n".format(sseqid, sstart, send, targetSequence[sstart - 1: send]))
			# pepAdjusted.write(">{0} [{1}:{2}] adjusted translation\n{3}\n".format(sseqid, sstart, send, str(Seq(targetSequence[sstart - 1: send], generic_dna).translate())))
			pepAdjusted.write(">{0} [{1}:{2}] adjusted translation\n{3}\n".format(sseqid, sstart, send, str(Seq(targetSequence[sstart - 1: send]).translate())))
			# if(args.verbose): sys.stdout.write(">{0} [{1}:{2}] adjusted\n{3}\n>{0} [{1}:{2}] adjusted translation\n{4}\n".format(sseqid, sstart, send, targetSequence[sstart - 1: send], str(Seq(targetSequence[sstart - 1: send], generic_dna).translate())))
			if(args.verbose): sys.stdout.write(">{0} [{1}:{2}] adjusted\n{3}\n>{0} [{1}:{2}] adjusted translation\n{4}\n".format(sseqid, sstart, send, targetSequence[sstart - 1: send], str(Seq(targetSequence[sstart - 1: send]).translate())))
			sstart, send, dna, pep = extendTranslation(targetSequence, sstart, send)
			dnaExpanded.write(">{0} [{1}:{2}] expanded\n{3}\n".format(sseqid, sstart, send, dna))
			pepExpanded.write(">{0} [{1}:{2}] expanded translation\n{3}\n".format(sseqid, sstart, send, pep))
			if(args.verbose): sys.stdout.write(">{0} [{1}:{2}] expanded\n{3}\n>{0} [{1}:{2}] expanded translation\n{4}\n".format(sseqid, sstart, send, dna, pep))
			if(args.verbose): sys.stdout.write("#\n")
	if(args.verbose): sys.stdout.write("##\n")
	# Close output files:
	dnaHits.close()
	dnaAdjusted.close()
	pepAdjusted.close()
	dnaExpanded.close()
	pepExpanded.close()
	return

##
# Execute functions
##

results_dic = parse_blast_results(args.blast_results)

if(args.genes_table):
	genes_dic = parse_genes(args.genes_table)
	report_basic_results(genes_dic, results_dic)

pos = get_overlapping_positions(results_dic)

best = get_best_hits_per_position(results_dic, pos)

extractUniqueOrfs(best, args.target)



exit() # Quit this script
