#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# GitLab: MachadoDJ & DachBrown

# bestHitsperQuery.py
# Gets a .tsv table of results from Blast (output format 6) and returns the N best hits per query.

##
# Import libraries
##

import argparse, re, sys

try:
	from Bio import SeqIO
	from Bio.Seq import Seq
	from Bio.Alphabet import generic_dna
except:
	sys.stderr.write("! ERROR: Could not load Biopython.\n")
	exit()

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-b", "--blast_results", help = "BLAST results in output format 6", type = str)
parser.add_argument("-e", "--evalue", help = "E-value threshold (default = 1e-03)", type = float, default = 1e-03)
parser.add_argument("-n", "--number", help = "Number of best hits per query (default = 1)", type = int, default = 1)
parser.add_argument("-s", "--sorting_order", help = "Space-separeted list of Columns to be sorted, in order, starting from 0 (default = 3 2 11 10)", type = int, nargs = "+", default = [3, 2, 11, 10])
args = parser.parse_args()

##
# Define functions
##



def parse_blast_results(filename, threshold):
	dic = {}
	handle = open(filename, "r")
	for line in handle.readlines():
		line = re.sub("#.*", "", line.strip())
		if(line):
			line = line.split("\t")
			try:
				qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8], line[9], line[10], line[11]
				pident = float(pident)
				length = int(length)
				mismatch = int(mismatch)
				gapopen = int(gapopen)
				qstart = int(qstart)
				qend = int(qend)
				sstart = int(sstart)
				send = int(send)
				evalue = -1 * float(evalue) # This multiplication helps with sorting later
				bitscore = float(bitscore)
			except:
				sys.stderr.write("! Error parsing file {}.\n".format(filename))
				exit()
			else:
				# sys.stdout.write("{}\n".format(qseqid))
				if(qseqid in dic and float(evalue) <= threshold):
					try:
						qseqid = re.compile(".+\|(.+)\|.+").findall(qseqid)[0]
					except:
						pass
					dic[qseqid] += [[qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore]]
				elif(float(evalue) <= threshold):
					try:
						qseqid = re.compile(".+\|(.+)\|.+").findall(qseqid)[0]
					except:
						pass
					dic[qseqid] = [[qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore]]
				else:
					sys.stderr.write("REJECTED:\n{}\n".format("\t".join([qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore])))
					pass
	handle.close()
	return dic




def get_best_hits_per_position(results_dic, n):
	best = []
	for qseqid in results_dic:
		hits = [i for i in results_dic[qseqid]]
		for i in args.sorting_order:
			hits.sort(key = lambda x: x[i], reverse = True)
		results_dic[qseqid] = hits[:n]
	return results_dic



def report(results_dic):
	sys.stdout.write("# qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\n")
	counter = 0
	for qseqid in sorted(results_dic):
		counter += 1
		for line in results_dic[qseqid]:
			line = [str(val) for val in line]
			line = "\t".join(line)
			sys.stdout.write("{}\n".format(line))
	return


##
# Execute functions
##

results_dic = parse_blast_results(args.blast_results, args.evalue)
results_dic = get_best_hits_per_position(results_dic, args.number)
report(results_dic)




exit() # Quit this script
