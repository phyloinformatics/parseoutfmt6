#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# GitLab: MachadoDJ & DachBrown

# bestHitPerPositionPerQuery.py
# Gets a .tsv table of results from Blast (output format 6) and filter it so only the best one hit per position per query (NOT SUBJECT!) are shown.

##
# Import libraries
##

import argparse, re, sys

try:
	from Bio import SeqIO
	from Bio.Seq import Seq
	from Bio.Alphabet import generic_dna
except:
	sys.stderr.write("! ERROR: Could not load Biopython.\n")
	exit()

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-b", "--blast_results", help = "BLAST results in output format 6", type = str)
parser.add_argument("-e", "--evalue", help = "E-value threshold (default = 1e-03)", type = float, default = 1e-03)
parser.add_argument("-s", "--sorting_order", help = "Space-separeted list of Columns to be sorted, in order, starting from 0 (default = 3 2 11 10)", type = int, nargs = "+", default = [3, 2, 11, 10])
parser.add_argument("-v", "--verbose", help="Increase output verbosity", action="store_true")
args = parser.parse_args()

##
# Define functions
##


# REVIWED
def parse_blast_results(filename):
	dic = {}
	handle = open(filename, "r")
	for line in handle.readlines():
		line = re.sub("#.*", "", line.strip())
		if(line):
			line = line.split("\t")
			try:
				qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8], line[9], line[10], line[11]
				pident = float(pident)
				length = int(length)
				mismatch = int(mismatch)
				gapopen = int(gapopen)
				qstart = int(qstart)
				qend = int(qend)
				sstart = int(sstart)
				send = int(send)
				evalue = -1 * float(evalue) # This multiplication helps with sorting later
				bitscore = float(bitscore)
			except:
				sys.stderr.write("! Error parsing file {}.\n".format(filename))
				exit()
			else:
				# sys.stdout.write("{}\n".format(qseqid))
				if(qseqid in dic and float(evalue) <= args.evalue):
					try:
						qseqid = re.compile(".+\|(.+)\|.+").findall(qseqid)[0]
					except:
						pass
					dic[qseqid] += [[qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore]]
				elif(float(evalue) <= args.evalue):
					try:
						qseqid = re.compile(".+\|(.+)\|.+").findall(qseqid)[0]
					except:
						pass
					dic[qseqid] = [[qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore]]
				else:
					sys.stderr.write("REJECTED:\n{}\n".format("\t".join([qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore])))
					pass
	handle.close()
	return dic


# REVIWED
def get_overlapping_positions(results_dic):
	if(args.verbose): sys.stdout.write("## Positions:\n")
	pos = {}
	for qseqid in results_dic:
		dic_per_sseqid = {}
		for list in results_dic[qseqid]:
			qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = list
			s = int(qstart)
			e = int(qend)
			add = True
			for key in dic_per_sseqid:
				i = dic_per_sseqid[key][0]
				j = dic_per_sseqid[key][1]
				x = range(min(s, e), max(s, e) + 1)
				y = range(min(i, j), max(i, j) + 1)
				xs = set(x)
				if(len(xs.intersection(y)) >= 1):
					add = False
					dic_per_sseqid[key] = [min(s, i), max(e, j)]
					break
			if(add == True):
				dic_per_sseqid[len(dic_per_sseqid)] = [s, e]
		pos[qseqid] = dic_per_sseqid
		if(args.verbose): sys.stdout.write("# {} {}\n".format(qseqid, dic_per_sseqid))
	if(args.verbose): sys.stdout.write("##\n")
	return pos



def get_best_hits_per_position(results_dic, pos):
	best = []
	if(args.verbose): sys.stdout.write("## Best hits per position:\n# qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\n")
	for qseqid in results_dic:
		hits = [i for i in results_dic[qseqid]]
		for i in args.sorting_order:
			hits.sort(key = lambda x: x[i], reverse = True)
		for key in pos[qseqid]:
			i, j = pos[qseqid][key]
			for line in hits:
				s = line[6]
				e = line[7]
				x = range(min(s, e), max(s, e) + 1)
				y = range(min(i, j), max(i, j) + 1)
				xs = set(x)
				if(len(xs.intersection(y)) >= 1):
					try:
						line[10] = "{:.2e}".format(-1 * line[10]) # This helps reading the results
					except:
						pass
					if(not line in best):
						best.append(line)
						sys.stdout.write("{}\n".format("\t".join([str(k) for k in line])))
					break
	if(args.verbose): sys.stdout.write("##\n")
	return best


##
# Execute functions
##

results_dic = parse_blast_results(args.blast_results)

pos = get_overlapping_positions(results_dic)

best = get_best_hits_per_position(results_dic, pos)



exit() # Quit this script
