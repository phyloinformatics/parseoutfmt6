#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# GitLab: MachadoDJ

# splitTextFile
# Use this to split text files in smaller files with up to a certain number of lines

##
# Import libraries
##

import argparse, re, sys

try:
	from Bio import SeqIO
	from Bio.Seq import Seq
	from Bio.Alphabet import generic_dna
except:
	sys.stderr.write("! ERROR: Could not load Biopython.\n")
	exit()

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()

parser.add_argument("-i", "--input", help = "Path to input text file", type = str)
parser.add_argument("-l", "--lines", help = "The input text file will be split into smaller text files up to this many lines (default = 100000)", type = int, default = 100000)
parser.add_argument("-p", "--prefix", help = "Prefix for naming output text files (default = out)", type = str, default = "output")
args = parser.parse_args()

##
# Define functions
##

def report(o, p, c):
	outfile = open("{}_{}.txt".format(p, c), "w")
	outfile.write(o)
	outfile.close()
	return

def main(f, n, p):
	countf = 0
	countl = 0
	output = ""
	handle = open(f, "r")
	for line in handle.readlines():
		line = line.strip()
		if(line):
			output += "{}\n".format(line)
			countl += 1
			if countl == n:
				countl = 0
				countf += 1
				report(output, p, countf)
				output = ""
	handle.close()
	if output:
		countf += 1
		report(output, p, countf)
	return

##
# Execute functions
##

main(args.input, args.lines, args.prefix)



exit() # Quit this script
