# parseOutfmt6

## Summary

1. Gets a "tab-separated values" (TSV) table of results from Blast (output format 6; `outfmt6`) and a genes TSV table as well.
2. Returns best hit per position (unique overlapping positions per `sseqid`).
3. Additionally, search for translatable sequences within the uniq hits.

## Assumptions

- This program was written to parse BLAST’s `outfmt6` tables resulting from searches in with know query sequences were aligned against the user’s target (also called subject) database. For example, the target database can be a series of recently assembled scaffolds and the query sequences may be several genes of interest
- The target sequence (i.e, subject or database) is assumed to be DNA, translatable using the standard codon table
- Hits are suppose to represent possible protein coding sequences

## Dependencies

- Python (tested with Python v3.6.8, distributed with Anaconda)
- [Biopython](https://biopython.org/)

### Installing Biopython

If you don't have Biopython, the easiest way to get it is using `pip`:

```bash
pip install biopython
```

For more details on how to install Biopython, visit https://biopython.org/wiki/Download.

## Input Files

- Blast results in output format 6 (tab-separated values; TSV)
- Target (subject) sequence(s) in FASTA format

### Non-required input files and arguments

- Genes table in TSV format (Accession, Nickname, and Description)
- E-value threshold (`-e` or `--evalue`)
- Sorting order (column numbers, starting from 0, separated by spaces)

## Help

```
usage: parseOutfmt6.py [-h] [-b BLAST_RESULTS] [-e EVALUE] [-g GENES_TABLE]
                       [-o OUTPUT] [-s SORTING_ORDER [SORTING_ORDER ...]]
                       [-t TARGET]

optional arguments:
  -h, --help            show this help message and exit
  -b BLAST_RESULTS, --blast_results BLAST_RESULTS
                        BLAST results in output format 6
  -e EVALUE, --evalue EVALUE
                        E-value threshold (default = 1e-03)
  -g GENES_TABLE, --genes_table GENES_TABLE
                        Gene information in tab-separated values (Accession,
                        Nickname, and Description). Not required, used to
                        generate basic statistics only.
  -o OUTPUT, --output OUTPUT
                        Prefix for output files (default = 'output')
  -s SORTING_ORDER [SORTING_ORDER ...], --sorting_order SORTING_ORDER [SORTING_ORDER ...]
                        Space-separeted list of Columns to be sorted, in
                        order, starting from 0 (default = 3 2 11 10)
  -t TARGET, --target TARGET
                        Path to target DNA sequence(s) in FASTA format
```

## Output

All main results are printed into the standard output (`stdout`). Additionaly, several milti-FASTA files are created:

- `*_hits_dna.fasta`: The best uniq hits per position (start and end position, orientation, and read frame may be off)
- `*_adjusted_dna.fasta`: The best uniq hits per position (adusted so that the sequence's length is divisible by 3)
- `*_adjusted_pep.fasta`: The translations of sequences that are in `*_adjusted_dna.fasta`
- `*_expanded_dna.fasta`: Expanded sequences (from `Met` to `Stop`)
- `*_expanded_pep.fasta`: The translations of sequences that are in `*_expanded_dna.fasta`

### Note on "expanded" sequences

The program try to expand sequences until a metionine is found on the 5' end of the sequence and a stop codon is foung on the 3' end of the sequence.

## Example Command Line

```bash
python3 parseOutfmt6.py -g example/genes.tsv -b example/results.tsv -t example/target.fasta > output.txt
```

## Auxiliary scripts

A few auxiliary scripts have been added since Feb. 19, 2020. See the `auxiliary` directory. All auxiliary scripts are written in Python v3+, and the user may use the argument `-h` or `--help` to get details about how to use each one of them.